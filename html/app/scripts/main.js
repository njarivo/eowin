
function scrollShow(element, limit = 400, toggler) {
  if (element.length) {
    $(window).on('scroll', function () {
      var currentTop = $(document).scrollTop();
      if (limit < currentTop) {
        element.addClass(toggler);
      } else if (element.hasClass(toggler)) {
        element.removeClass(toggler);
      }
    });
  }
}

function sliderMobile(slider, setting, breakpointValue = false) {
  const breakpoint = window.matchMedia(breakpointValue);
  let swiperMobile;
  const breakpointChecker = function () {
    if (breakpoint.matches === true) {
      if (swiperMobile !== undefined) {
        swiperMobile.destroy(true, true);
        return;
      }
    } else if (breakpoint.matches === false) {
      return enableSwiper();
    }
  };
  const enableSwiper = function () {
    swiperMobile = new Swiper(slider, setting);
  };

  breakpoint.addEventListener('change', breakpointChecker);

  breakpointChecker();
}

function eowinSlider(slider, setting) {
  new Swiper(slider, setting);
}


const app = {
  textarea() {
    autosize(document.querySelectorAll('textarea'));
  },
  menu() {
    const toggler = document.querySelector('.toggler');
    const navbarNav = document.querySelector('.navbar-nav');
    const headerIconLoupe = document.querySelector('.header-icon-loupe');
    const body = document.querySelector('body');
    toggler.addEventListener('click', function () {
      navbarNav.classList.toggle('show');
      headerIconLoupe.classList.toggle('d-none');
      body.classList.toggle('overflow-hidden');
    });

    //humaburgers
    const hamburgers = document.querySelectorAll('.hamburger');
    if (hamburgers.length) {
      [].forEach.call(hamburgers, function (hamburger) {
        hamburger.addEventListener('click', function () {
          this.classList.toggle('is-active');
        }, false);
      });
    }
    scrollShow($('.header'), 10, 'scrolled');

    //submenus
    const navItems = document.querySelectorAll('.nav-item');
    const navbar = document.querySelector('.navbar');
    let height = 0;
    if (navItems.length) {
      navItems.forEach(function (navItem) {
        navItem.addEventListener('mouseover', function () {
          if(this.nextElementSibling !== null) {
            height = this.nextElementSibling.offsetHeight;
            navbar.style.height = height + 75 +'px';
            navbar.style.background = '#1d637c';
          }
        });
        navItem.addEventListener('mouseleave', function () {
          navbar.style.height = '0px';
          navbar.style.background = 'transparent';
        });
      });
    }

    const submenus = document.querySelectorAll('.submenu');
    if (submenus.length) {
      submenus.forEach(function (submenu) {
        submenu.addEventListener('mouseover', function () {
          navbar.style.height = height+ 75 +'px';
          navbar.style.background = '#1d637c';
        });
        submenu.addEventListener('mouseleave', function () {
          navbar.style.height = '0px';
          navbar.style.background = 'transparent';
        });
      });
    }
  },
  homeSlider() {
    const slider = '.brand-home-slider';
    const setting = {
      autoplay: {
        delay: 5000,
        disableOnInteraction: false
      },
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    };
    eowinSlider(slider, setting);
  },
  actuSlider() {
    const slider = '.actu-slider';
    const setting = {
      loop: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      }
    };
    eowinSlider(slider, setting);
  },

  sliderArticle() {
    const slider = '#actu-article-slider-container';
    const breakpointValue = '(min-width:768px)';
    const setting = {
      autoplay: {
        delay: 30000,
        disableOnInteraction: false
      },
      loop: true,
      spaceBetween: 20,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
    };
    sliderMobile(slider, setting, breakpointValue);
  },
}


$(function () {
  AOS.init({
    offset: 50,
    duration: 1000,
  });

  app.textarea();
  app.menu();
  app.homeSlider();
  app.actuSlider();
  app.sliderArticle();
});
